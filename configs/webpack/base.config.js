const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const tsconfigPath = path.resolve(__dirname, '../../tsconfig.json');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
	name: 'base',
	entry: './index.ts',
	context: path.resolve(__dirname, '../../src'),
	resolve: {
		extensions: ['.ts', '.js', '.vue'],
		alias: {
			Stylesheets: path.resolve(__dirname, '../../src/stylesheets/'),
			Main: path.resolve(__dirname, '../../src/'),
			Components: path.resolve(__dirname, '../../src/components'),
			Shared: path.resolve(__dirname, '../../src/components/Shared'),
			Assets: path.resolve(__dirname, '../../src/assets')
		}
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
			},
			{
				test: /\.(graphql|gql)$/,
				exclude: /node_modules/,
				loader: 'graphql-tag/loader'
			},
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: [
					'cache-loader',
					{
						loader: 'thread-loader',
						options: {
							workers: require('os').cpus().length - 1
						}
					},
					{
						loader: 'ts-loader',
						options: {
							appendTsSuffixTo: [/\.ts\.vue$/],
							appendTsxSuffixTo: [/\.tsx\.vue$/],
							configFile: tsconfigPath,
							happyPackMode: true
						}
					}
				]
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				loaders: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[hash].[ext]'
						}
					},
					{
						loader: 'img-loader'
					}
				]
			}
		]
	},
	plugins: [
		new VueLoaderPlugin(),
		new HtmlWebpackPlugin({
			title: require('../../package.json').name,
			template: 'index.html'
		}),
		new ForkTsCheckerWebpackPlugin({
			checkSyntacticErrors: true,
			async: false,
			tsconfig: tsconfigPath
		})
	]
};
