import './Post.style.sass';
import Vote from 'Shared/Vote/Vote.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Vote
	},
	props: {
		title: String,
		author: Object,
		createdOn: String,
		id: String
	}
})
export default class Post extends Vue {
	protected showPost() {
		this.$store.state.postWindow = !this.$store.state.postWindow;
	}
}
