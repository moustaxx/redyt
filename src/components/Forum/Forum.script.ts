import './Forum.style.sass';
import Post from './Post/Post.vue';
import LoadingGif from 'Shared/LoadingGif/LoadingGif.vue';

import Vue from 'vue';
import Component from 'vue-class-component';
import { GET_POSTS } from './Forum.apollo';

@Component({
	components: {
		Post,
		LoadingGif
	},
})
export default class Forum extends Vue {
	protected GET_POSTS = GET_POSTS;
	protected subforumID: string | any = null;
	
	protected async beforeMount() {
		this.subforumID = this.$store.state.subforum.ID;
	}
}
