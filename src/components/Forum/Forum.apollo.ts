import gql from 'graphql-tag';

export const GET_POSTS = gql`
	query($subforumID: ID!) {
		getPostsBySubforum(
			subforum: $subforumID
		){
			id
			title
			content
			author {
				name
				id
			}
			createdOn
		}
	}
`;
