import gql from 'graphql-tag';

export const INFO_QUERY = gql`
	query {
		info
	}
`;
