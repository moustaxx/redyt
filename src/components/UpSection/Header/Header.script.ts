import './Header.style.sass';
import Vue from 'vue';
import Component from 'vue-class-component';
import { INFO_QUERY } from './Header.apollo';


@Component({
	components: {
	}
})

export default class Header extends Vue {
	protected INFO_QUERY = INFO_QUERY;
}
