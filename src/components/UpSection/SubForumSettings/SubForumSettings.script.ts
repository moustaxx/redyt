import './SubForumSettings.style.sass';
import SortMenu from './SortMenu/SortMenu.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		SortMenu
	}
})

export default class SubForumSettings extends Vue {
	public isSortOpen = false;
}
