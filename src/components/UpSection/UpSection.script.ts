import './UpSection.style.sass';
import Bookmarks from './Bookmarks/Bookmarks.vue';
import Header from './Header/Header.vue';
import SubForumSettings from './SubForumSettings/SubForumSettings.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Bookmarks,
		Header,
		SubForumSettings
	}
})

export default class UpSection extends Vue {
}
