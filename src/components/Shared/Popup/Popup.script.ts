import './Popup.style.sass';
import Vue from 'vue';
import Component from 'vue-class-component';
import { VERIFY_LOGIN } from './Popup.apollo';

@Component({
	components: {
	},
})
export default class Popup extends Vue {
	public usernameInput = '';
	public passwordInput = '';
	public loginError = false;
	public loading = false;
	public success = false;

	protected async handleSubmit() {
		console.log('Login:', this.usernameInput, 'Password:', this.passwordInput);
		this.loading = true;
		try {
			const result = await this.$apollo.query({
				query: VERIFY_LOGIN,
				variables: {
					name: this.usernameInput,
					password: this.passwordInput
				},
				fetchPolicy: 'no-cache'
			});
			this.loginError = false;
			this.success = true;
			localStorage.setItem('token', result.data.verifyLogin.token);
			this.$store.state.loggedIn = true;
			console.log(result.data.verifyLogin);
			setTimeout(() => {
				this.$router.go(0);
			}, 200);
		} catch (err) {
			if ((err as Error).message.includes('Invalid credentials')) {
				console.log('Invalid login or password', err);
				this.loginError = true;
				this.success = false;
				this.$store.state.loggedIn = false;
			}
			else console.log('Login error:', err);
		} finally {
			this.loading = false;
		}
	}

	protected closePostWindow() {
		this.$store.state.popup = false;
	}
	protected stopPropagation(event: any) {
		event.stopPropagation();
	}
}
