import './Aside.style.sass';
import CommunityDetails from './CommunityDetails/CommunityDetails.vue';
import Moderation from './Moderation/Moderation.vue';
import Rules from './Rules/Rules.vue';
import Moderators from './Moderators/Moderators.vue';
import Footer from './Footer/Footer.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		CommunityDetails,
		Moderation,
		Rules,
		Moderators,
		Footer,
	},
})
export default class Aside extends Vue {
}
