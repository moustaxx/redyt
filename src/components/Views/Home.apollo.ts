import gql from 'graphql-tag';

export const GET_SUBFORUM_ID = gql`
	query($name: String!) {
		getSubforum(name: $name) {
			id
			description
			colors {
				primary
				secondary
				tertiary
			}
		}
	}
`;
