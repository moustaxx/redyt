import TopBar from '../TopBar/TopBar.vue';
import UpSection from '../UpSection/UpSection.vue';
import Forum from '../Forum/Forum.vue';
import Aside from 'Shared/Aside/Aside.vue';
import Popup from 'Shared/Popup/Popup.vue';
import PostWindow from '../PostWindow/PostWindow.vue';

import { GET_SUBFORUM_ID } from './Home.apollo';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		TopBar,
		UpSection,
		Forum,
		Aside,
		PostWindow,
		Popup
	},
})

export default class Home extends Vue {
	protected loading = true;

	protected validateSubforum = async (subforumName: any) => {
		try {
			const result = await this.$apollo.query({
				query: GET_SUBFORUM_ID,
				variables: {
					name: subforumName
				}
			});
			this.resolveColors(result.data.getSubforum.colors);
			this.$store.state.subforum.ID = result.data.getSubforum.id;
			this.$store.state.subforum.description = result.data.getSubforum.description;
			this.$store.state.subforum.name = subforumName;
		} catch (err) {
			console.error('Subforum not found!', err);
			this.$store.state.subforum.error = true;
		}
	}

	protected async resolveColors({ primary, secondary, tertiary }: any) {
		const style = `
			<style>
				:root {
					--main-subforum-color: ${primary};
					--secondary-subforum-color: ${secondary};
					--tertiary-subforum-color: ${tertiary};
				}
			</style>
		`;
		window.document.head.innerHTML += style;
	}

	protected get showPopup() {
		return this.$store.state.popup;
	}

	protected async beforeMount() {
		const subforumName = this.$route.params.subForumName;
		await this.validateSubforum(subforumName);
		this.loading = false;
	}
}
