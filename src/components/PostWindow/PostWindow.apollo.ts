import gql from 'graphql-tag';

export const GET_POST = gql`
	query($postID: ID!) {
		getPostByID(
			id: $postID
		){
			id
			title
			author {
				id
				name
			}
			content
			createdOn
		}
	}
`;
