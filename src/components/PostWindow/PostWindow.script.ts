import './PostWindow.style.sass';
import Aside from 'Shared/Aside/Aside.vue';
import Header from './Header/Header.vue';
import Left from './Left/Left.vue';
import { GET_POST } from './PostWindow.apollo';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Aside,
		Header,
		Left
	},
})
export default class PostWindow extends Vue {
	public GET_POST = GET_POST;
	public postID = '';
	
	protected mounted() {
		this.asyncData();
	}

	protected async asyncData() {
		const postID = this.$route.params.postID;
		this.postID = postID;
	}

	protected closePostWindow() {
		this.$router.push({ path: `/r/${this.$route.params.subForumName}`});
	}
	protected stopPropagation(event: any) {
		event.stopPropagation();
	}
}
