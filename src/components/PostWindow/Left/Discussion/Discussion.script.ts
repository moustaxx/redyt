import './Discussion.style.sass';
import SortMenu from './SortMenu/SortMenu.vue';
import Comment from './Comment/Comment.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		SortMenu,
		Comment
	},
})
export default class Discussion extends Vue {
	public isSortOpen = false;
}
