import './Comment.style.sass';
import Vote from 'Shared/Vote/Vote.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Vote,
		Comment
	},
})
export default class Comment extends Vue {
	public isReplied = false;
}
