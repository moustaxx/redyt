import './Left.style.sass';
import Discussion from './Discussion/Discussion.vue';
import Vote from 'Shared/Vote/Vote.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Discussion,
		Vote
	},
	props: {
		title: String,
		content: String,
		author: Object,
		createdOn: String
	}
})
export default class Left extends Vue {
}
