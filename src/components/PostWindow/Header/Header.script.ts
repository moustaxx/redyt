import './Header.style.sass';
import Vote from 'Shared/Vote/Vote.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Vote
	},
	props: {
		title: String
	}
})
export default class Header extends Vue {
	protected hidePost() {
		this.$router.push({ path: '/', params: { subForumName: this.$route.params.subForumName } });
	}
}
