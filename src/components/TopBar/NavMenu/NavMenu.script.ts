import './NavMenu.style.sass';
import Navigation from './Navigation/Navigation.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Navigation
	}
})

export default class NavMenu extends Vue {
	public navMenu = false;
}
