import './SearchBox.style.sass';
import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
	}
})

export default class SearchBox extends Vue {
	public userInput = '';
	protected search() {
		this.$router.push({ query: { search: this.userInput } });
	}
}
