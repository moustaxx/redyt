import './TopBar.style.sass';
import Logo from './Logo/Logo.vue';
import SearchBox from './SearchBox/SearchBox.vue';
import NavMenu from './NavMenu/NavMenu.vue';
import UserArea from './UserArea/UserArea.vue';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
	components: {
		Logo,
		SearchBox,
		NavMenu,
		UserArea
	}
})

export default class TopBar extends Vue {
	protected showPopup() {
		this.$store.state.popup = true;
	}
}
