import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './components/Views/Home.vue';
import PageNotFound from './components/Views/PageNotFound.vue';
import PostWindow from './components/PostWindow/PostWindow.vue';

Vue.use(VueRouter);
const routes = [
	{ path: '/', redirect: '/r/MaterialDesign' },
	{ path: '/r/:subForumName', component: Home,
		children: [
			{
				// Will be rendered inside <router-view>
				path: 'posts/:postID',
				component: PostWindow
			}
		]
	},
	{ path: '*', component: PageNotFound }
];
const router = new VueRouter({ mode: 'history', routes });

export default router;
