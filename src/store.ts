import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		popup: false,
		subforum: {
			ID: null,
			name: null,
			error: false,
			description: null
		}
	}
});

export default store;
